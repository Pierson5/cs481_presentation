import 'package:flutter/material.dart';
import 'package:flutter_presentation/Widgets/Buttons1Widget.dart';
import 'package:flutter_presentation/Widgets/Buttons2Widget.dart';
import './Widgets/ChipsWidget.dart';
import './Widgets/DataTableWidget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.navigation),
        backgroundColor: Colors.green,
      ),
      appBar: AppBar(title: Text("Group 3 Presentation")),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: ListView(
            padding: EdgeInsets.fromLTRB(1.0,15,1,15),
            children: <Widget>[
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white,
                  elevation: 10,
                  child: Column(children: <Widget>[
                    Padding(
                        padding: EdgeInsets.all(20.0), child: Text("Card"))
                  ])),
              ButtonsCard(),
              moreButtons(),

              chipsCard(),
              DataTableCard()
            ]),
      ),
    );
  }
}

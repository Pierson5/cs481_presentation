import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class moreButtons extends StatefulWidget {
  @override
  _moreButtonsState createState() => _moreButtonsState();
}

class _moreButtonsState extends State<moreButtons> {
  @override
  Widget build(BuildContext context) {
    return Card(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        color: Colors.white,
        elevation: 10,
        child: Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Text("More Buttons"),
          ),
          myPopMenu(),
          myOutlineButton(),
          myIconButton(),
        ]));
  }
}

Widget myGridView() {
  return GridView.count(
      crossAxisCount: 2,
      padding: EdgeInsets.all(15),
      crossAxisSpacing: 15,
      mainAxisSpacing: 5,
      children: <Widget>[
        Container(
            color: Colors.indigo,
            child: Stack(
              children: <Widget>[
                Positioned(
                  child: myPopMenu(),
                  right: 0,
                )
              ],
            )),
        Container(
            color: Colors.lightBlue,
            child: Stack(
              children: <Widget>[
                Positioned(
                  child: myOutlineButton(),
                  right: 0,
                )
              ],
            )),
        Container(
            color: Colors.deepPurple,
            child: Stack(
              children: <Widget>[
                Positioned(
                  child: myIconButton(),
                  right: 0,
                )
              ],
            )),
      ]);
}

Widget myPopMenu() {
  return PopupMenuButton(
      itemBuilder: (context) => [
            PopupMenuItem(
                value: 1,
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                      child: Icon(Icons.print),
                    ),
                    Text('Print')
                  ],
                )),
            PopupMenuItem(
                value: 2,
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                      child: Icon(Icons.share),
                    ),
                    Text('Share')
                  ],
                )),
            PopupMenuItem(
                value: 3,
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                      child: Icon(Icons.add_circle),
                    ),
                    Text('Add')
                  ],
                )),
          ]);
}

Widget myOutlineButton() {
  return OutlineButton(
    onPressed: () {
      print('Received click');
    },
    child: Text('Click Me'),
    highlightedBorderColor: Colors.white,
  );
}

Widget myIconButton() {
  return IconButton(
    onPressed: () {
      print("Cake!");
    },
    icon: Icon(Icons.cake),
    color: Colors.red,
  );
}

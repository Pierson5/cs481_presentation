import 'package:flutter/material.dart';

class ButtonsCard extends StatefulWidget {
  @override
  _ButtonsCardState createState() => _ButtonsCardState();
}

class _ButtonsCardState extends State<ButtonsCard> {
  GlobalKey<ScaffoldState> _key;

  @override
  Widget build(BuildContext context) {
    return Card(
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        color: Colors.white,
        elevation: 10,
        child: Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Text("Buttons"),
          ),
          getButtons(),
          getDropdownButton(),
        ]));
  }
}

class getButtons extends StatefulWidget {
  @override
  _getButtonsState createState() => _getButtonsState();
}

class _getButtonsState extends State<getButtons> {

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: [
      Container(
          padding: EdgeInsets.all(20),
          alignment: Alignment.center,
          child: ButtonBar(
              alignment: MainAxisAlignment.center,
              buttonHeight: 4.0,
              children: <Widget> [
                FlatButton(
                    color: Colors.amber,
                    child: Text('yes', style: TextStyle(color: Colors.white),),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    onPressed: () {
                      showDialog(
                        context: context,
                        barrierDismissible: true,
                        builder: (context) {
                          return AlertDialog(
                              title: Text('Hello!'),
                              content: Text('You picked: Yes!'));},
                      );
                    }
                ),
                RaisedButton(
                    color: Colors.lime,
                    splashColor: Colors.pink,
                    child: Text('No'),
                    onLongPress: () {
                      showDialog(
                        context: context,
                        barrierDismissible: true,
                        builder: (context) {
                          return AlertDialog(
                              title: Text('Hello!'),
                              content: Text('You picked: No!'));},
                      );
                    }
                )
              ]
          )
      )]);
  }
}

class getDropdownButton extends StatefulWidget {
  @override
  _getDropdownButtonState createState() => _getDropdownButtonState();
}

class _getDropdownButtonState extends State<getDropdownButton> {
  String dropdownValue = 'One';

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      icon: Icon(Icons.arrow_downward, color: Colors.deepOrangeAccent),
      iconSize: 13,
      elevation: 16,
      style: TextStyle(color: Colors.deepOrangeAccent),
      underline: Container(
        height: 2,
        width: 4,
        color: Colors.deepOrangeAccent,
      ),
      onChanged: (String newValue) {
        setState(() {
          dropdownValue = newValue;
        });
        print(dropdownValue);
      },
      items: <String>['One', 'Two', 'Three', 'Four']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}



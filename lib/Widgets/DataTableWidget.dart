import 'package:flutter/material.dart';

class DataTableCard extends StatefulWidget {
  @override
  _dataTableCardState createState() => _dataTableCardState();
}

class _dataTableCardState extends State<DataTableCard> {
  GlobalKey<ScaffoldState> _key;

  @override
  Widget build(BuildContext context) {
    return Card(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        color: Colors.white,
        elevation: 10,
        child: Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Text("Data Table"),
          ),
          createDataTable(),
        ]));
  }
}

//Create Data Table-------------------------------------------------------------
class createDataTable extends StatefulWidget {
  @override
  _createDataTableState createState() => _createDataTableState();
}

class _createDataTableState extends State<createDataTable> {
  String label = "Snack Bar";
  Color background = Colors.indigo;
  Color labelColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columns: const <DataColumn>[
        DataColumn(
          label: Text(
            'Name',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            'Age',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            'Role',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Lucas')),
            DataCell(Text('21')),
            DataCell(Text('Student')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Kyle')),
            DataCell(Text('not so much')),
            DataCell(Text('Professor')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('William')),
            DataCell(Text('27')),
            DataCell(Text('Associate Professor')),
          ],
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';

class chipsCard extends StatefulWidget {
  @override
  _chipsCardState createState() => _chipsCardState();
}

class _chipsCardState extends State<chipsCard> {
  GlobalKey<ScaffoldState> _key;

  @override
  Widget build(BuildContext context) {
    return Card(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        color: Colors.white,
        elevation: 10,
        child: Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Text("Chips"),
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: wrapWidget(),
          ),
          actionChipName(),
          wrapFilter(),
          theInputChip(),
          choiceFruit(),
        ]));
  }
}

//Chip--------------------------------------------------------------------------
Widget chipName(String label, Color color) {
  return Chip(
    labelPadding: EdgeInsets.all(1.0),
    avatar: CircleAvatar(
      backgroundColor: Colors.grey.shade700,
      child: Text(
        label[0].toUpperCase() + label[1].toLowerCase(),
        style: TextStyle(
          fontSize: 12,
          fontStyle: FontStyle.italic,
        ),
      ),
    ),
    label: Text(
      label,
      style: TextStyle(
        color: Colors.black,
      ),
    ),
    backgroundColor: color,
    elevation: 2.0,
    shadowColor: Colors.black,
    padding: EdgeInsets.all(6.0),
  );
}

wrapWidget() {
  return Wrap(
    //Set spacing between chips horizontally
    spacing: 5.0,
    //Set spacing between chips vertically
    runSpacing: 1.0,
    direction: Axis.horizontal,
    children: <Widget>[
      chipName('Alice', Colors.red),
      chipName('Bob', Colors.greenAccent),
      chipName('Chuck', Colors.orange),
      chipName('Craig', Colors.deepPurpleAccent),
      chipName('Grace', Colors.lightBlueAccent),
      chipName('Walter', Colors.yellow),
    ],
  );
}

//Action-Chip-------------------------------------------------------------------
class actionChipName extends StatefulWidget {
  @override
  _actionChipNameState createState() => _actionChipNameState();
}

class _actionChipNameState extends State<actionChipName> {
  String label = "Snack Bar";
  Color background = Colors.indigo;
  Color labelColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      ActionChip(
        labelPadding: EdgeInsets.all(1.0),
        avatar: CircleAvatar(
          backgroundColor: Colors.grey.shade700,
          child: Text(
            label[0].toUpperCase() + label[1].toLowerCase(),
            style: TextStyle(
              fontSize: 12,
              fontStyle: FontStyle.italic,
            ),
          ),
        ),
        label: Text(
          label,
          style: TextStyle(
            color: labelColor,
          ),
        ),
        backgroundColor: background,
        elevation: 2.0,
        shadowColor: Colors.black,
        padding: EdgeInsets.all(6.0),
        onPressed: () {
          setState(() {
            final snackBar = SnackBar(
              content: Text("This isn't food ya know."),
              action: SnackBarAction(
                label: 'Close',
                onPressed: () {},
              ),
            );
            Scaffold.of(context).showSnackBar(snackBar);
          });
        },
      ),
    ]);
  }
}

//Filter-Chips------------------------------------------------------------------
class Label {
  const Label(this.name);

  final String name;
}

class wrapFilter extends StatefulWidget {
  @override
  _wrapFilterState createState() => _wrapFilterState();
}

class _wrapFilterState extends State<wrapFilter> {
//List to hold the chosen rooms in a string list
  List<String> _roomsFilter = <String>[];

//List with all the room strings to make filter chips from
  List<Label> _roomsList = <Label>[
    const Label('Hallway'),
    const Label('Garage'),
    const Label('Kitchen'),
    const Label('Living Room'),
    const Label('Bedroom'),
  ];

  Iterable<Widget> get roomsWidgets sync* {
    for (Label rooms in _roomsList) {
      yield Padding(
        padding: const EdgeInsets.all(1.0),
        child: FilterChip(
          avatar: CircleAvatar(
            child: Text(rooms.name[0].toUpperCase()),
          ),
          label: Text(rooms.name),
          selected: _roomsFilter.contains(rooms.name),
          onSelected: (bool selected) {
            setState(() {
              if (selected) {
                _roomsFilter.add(rooms.name);
              } else {
                _roomsFilter.removeWhere((String name) {
                  return name == rooms.name;
                });
              }
            });
          },
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Wrap(
        children: roomsWidgets.toList(),
      ),
      Text('Rooms: ${_roomsFilter.join(', ')}'),
    ]);
  }
}

//Input-Chip--------------------------------------------------------------------
class theInputChip extends StatefulWidget {
  @override
  _theInputChipState createState() => _theInputChipState();
}

class _theInputChipState extends State<theInputChip> {
  bool _isSelected = false;
  String label = "Fruit";

  @override
  Widget build(BuildContext context) {
    return InputChip(
      avatar: CircleAvatar(
        backgroundColor: Colors.grey.shade800,
        child: Text(label[0].toUpperCase()),
      ),
      label: Text(label),
      selected: _isSelected,
      selectedColor: Colors.redAccent,
      onSelected: (bool selected) {
        setState(() {
          _isSelected = selected;
        });
      },
      onDeleted: () {
        final snackBar = SnackBar(
          content: Text("You pressed the close button."),
        );
        Scaffold.of(context).showSnackBar(snackBar);
      },
    );
  }
}

//Choice-Chip-------------------------------------------------------------------
class choiceFruit extends StatefulWidget {
  @override
  _choiceFruitState createState() => _choiceFruitState();
}

class _choiceFruitState extends State<choiceFruit> {
  int _value = 0;
  List<Label> _fruitsList = <Label>[
    const Label('Dragonfruit'),
    const Label('Kiwi'),
    const Label('Pineapple'),
  ];

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: List<Widget>.generate(
        3,
        (int index) {
          return ChoiceChip(
            label: Text(_fruitsList.elementAt(index).name),
            selected: _value == index,
            onSelected: (bool selected) {
              setState(() {
                _value = selected ? index : null;
              });
            },
          );
        },
      ).toList(),
    );
  }
}
